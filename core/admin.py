from django.contrib import admin
from core.models import Client, Access, Analytic, Professional

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'active', 'description', )
    list_filter = ('active', )

admin.site.register(Client, ClientAdmin)
admin.site.register(Professional)
admin.site.register(Access)
admin.site.register(Analytic)
