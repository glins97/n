from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta


class Client(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=False)
    analytics = models.ManyToManyField('Analytic', blank=True, null=True)

    def __str__(self):
        return f'{self.name}'


class Analytic(models.Model):
    name = models.CharField(max_length=32, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'{self.name}'


class Access(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}::{self.client}'


class Person(models.Model):
    genders = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    name = models.CharField(max_length=200)
    gender = models.CharField(max_length=1, choices=genders)
    email = models.EmailField()

    def __str__(self):
        return self.name


class Professional(Person):
    specialty = models.CharField(max_length=32)
    client = models.ForeignKey(Client, models.CASCADE, blank=True, null=True, ) 
    duration = models.DurationField()
    interval = models.DurationField()
