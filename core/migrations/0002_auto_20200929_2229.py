# Generated by Django 3.0.10 on 2020-09-29 22:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='analytic',
            name='name',
            field=models.CharField(blank=True, max_length=32, null=True),
        ),
        migrations.AlterField(
            model_name='analytic',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
    ]
