# Generated by Django 3.0.10 on 2020-10-05 14:10

from django.db import migrations, models
from datetime import timedelta

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_professional'),
    ]

    operations = [
        migrations.AddField(
            model_name='professional',
            name='interval',
            field=models.DurationField(default=timedelta(minutes=30), verbose_name='Interval (minutes)'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='professional',
            name='duration',
            field=models.DurationField(default=timedelta(minutes=30), verbose_name='Duration (minutes)'),
        ),
    ]
