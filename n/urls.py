"""n URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.http import FileResponse
from chatbot.views import *
from chatbot.apis import *

from bauth.views import *
from bauth.apis import *

def get_service_worker(request):
    return FileResponse(open('service-worker.js', 'rb'))

urlpatterns = [
    # - bauth 
    path('login/', login_view),
    path('api/login/', login_endpoint),
    path('api/logout/', logout_endpoint),

    # - chatbot
    path('', analytics_view),
    path('/', analytics_view),
    path('analytics/', analytics_view),
    path('analytics/<str:timeframe>/', analytics_view),
    path('flows/', flows_view),
    path('flows/<int:id>/', flow_view),
    path('add/', add_flow_view),
    path('edit/', edit_flow_view),

    path('api/chatbot/add/', add_flow_endpoint),
    path('api/chatbot/edit/', edit_flow_endpoint),
    path('api/chatbot/get/flows/', get_flows_endpoint),
    path('api/chatbot/get/flows_activities/', get_flow_activities_endpoint),
    path('api/chatbot/get/handle/', handle_endpoint),
    path('api/chatbot/get/selenium_clients/', selenium_clients_endpoint),
    
    path('admin/', admin.site.urls),
    path('service-worker.js', get_service_worker),
]
