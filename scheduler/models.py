from django.db import models
from core.models import Person, Professional 
from scheduler.aux import add_event

class Reservee(Person):
    cpf = models.CharField(unique=True, max_length=32)
    phone = models.CharField(max_length=32)


class Reservation(models.Model):
    reservee = models.ForeignKey(Reservee, models.CASCADE)
    professional = models.ForeignKey(Professional, models.CASCADE)
    remind_client = models.BooleanField(default=True)
    event_added = models.BooleanField(default=False, editable=False)
    
    when = models.DateTimeField()
    previous_when = models.DateTimeField(blank=True, null=True, editable=False)

    class Meta:
        models.UniqueConstraint(fields=['when', 'professional'], name='unique_reservation')

    def __str__(self):
        return f'Reservation #{self.id}'

    def save(self, *args, **kwargs):
        if self.when and self.previous_when and self.when != self.previous_when:
            self.event_added = False
        
        # adds new event if its first save or if 'when' changed
        if not self.event_added:
            self.event_added = True
            self.previous_when = self.when
            add_event(
                self.professional.client, self.professional,
                self.reservee, self.professional.client.name, '',
                self.when.isoformat(), (self.when + self.professional.duration).isoformat())
        super(Reservation, self).save(*args, **kwargs)