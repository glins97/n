# Generated by Django 3.0.10 on 2020-10-05 14:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    atomic = False
    
    dependencies = [
        ('core', '0003_professional'),
        ('scheduler', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Booking',
            new_name='Reservation',
        ),
        migrations.RenameModel(
            old_name='Client',
            new_name='Reservee',
        ),
        migrations.AlterField(
            model_name='reservation',
            name='client',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Client'),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='professional',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Professional'),
        ),
        migrations.DeleteModel(
            name='Professional',
        ),
    ]
