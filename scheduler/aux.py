import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


class ServiceManager():
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    services = {} 
    
    def get_service(self, client):
        if client in self.services:
            return self.services[client]

        creds = None
        if os.path.exists(f'scheduler/creds/{client}.pickle'):
            with open(f'scheduler/creds/{client}.pickle', 'rb') as token:
                creds = pickle.load(token)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'scheduler/creds/credentials.json', self.SCOPES)
                creds = flow.run_local_server(port=0)
            with open(f'scheduler/creds/{client}.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.services[client] = build('calendar', 'v3', credentials=creds)
        return self.services[client]


def get_events(client, results=10):
    service = ServiceManager().get_service(client)

    now = datetime.datetime.utcnow().isoformat() + 'Z' 
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=results, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    return {event['summary']:event for event in events}


def add_event(client, professional, reservee, event_name, event_description, start_date, end_date):
    print('@add_event', client, professional, reservee, event_name, event_description, start_date, end_date)
    
    service = ServiceManager().get_service(client)
    
    event = {
        'summary': f'{client.name}',
        'location': f'{client.location}',
        'description': f'{event_description}',
        'start': {
            'dateTime': f'{start_date}-03:00',
            'timeZone': 'America/Sao_Paulo',
        },
        'end': {
            'dateTime': f'{end_date}-03:00',
            'timeZone': 'America/Sao_Paulo',
        },
        'attendees': [
            {'email': f'{professional.email}'},
            {'email': f'{reservee.email}'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
            {'method': 'email', 'minutes': 24 * 60},
            {'method': 'popup', 'minutes': 2 * 60},
            ],
        },
    }

    service.events().insert(calendarId='primary', body=event).execute()
    