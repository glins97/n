from core.models import *
from chatbot.manager.constants import CREATION, CHANGE
from chatbot.manager.manager import Manager

signals = [
    (Client.objects.get(name='Clínica Santa Gianna'), 'ATTENTION_REQUIRED', CREATION, lambda: print('SIGNAL RECEIVED: CREATION OF ATTENTION REQUIRED')),
    (Client.objects.get(name='Clínica Santa Gianna'), 'ATTENTION_REQUIRED', CHANGE, lambda: print('SIGNAL RECEIVED: CHANGE OF ATTENTION REQUIRED')),
]

def register():
    for client, signal, status, f in signals:
        Manager.register(client, signal, status, f)
