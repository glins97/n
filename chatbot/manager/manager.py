
class Manager(object):
    registers = {}

    @classmethod
    def register(self, client, sig, status, f):
        if client.id not in Manager.registers:
            Manager.registers[client.id] = {}
        
        if f'{sig}-{status}' not in Manager.registers[client.id]:
            Manager.registers[client.id][f'{sig}-{status}'] = f
            return True
        
        return False

    @classmethod
    def signal(self, event_activity, status):
        client = event_activity.flow_activity.flow.client
        sig = event_activity.event.signal

        if client.id not in Manager.registers:
            return False
        
        if f'{sig}-{status}' not in Manager.registers[client.id]:
            return False

        Manager.registers[client.id][f'{sig}-{status}']()
        return True
