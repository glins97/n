from django.contrib import admin
from .models import *


class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'signal', )
    search_fields = ('name', 'signal', )

    def client__name(self, obj):
        return obj.client.name


class EventActivityAdmin(admin.ModelAdmin):
    list_display = ('event', 'flow_activity', )
    search_fields = ('event', 'flow_activity', )


class FlowAdmin(admin.ModelAdmin):
    list_display = ('client__name', 'parent__description', 'description', )
    search_fields = ('client__name', 'parent__description', 'description', )

    def client__name(self, obj):
        return obj.client.name
        
    def parent__description(self, obj):
        return obj.parent.description if obj.parent else '-'


class FlowActivityAdmin(admin.ModelAdmin):
    list_display = ('flow', 'user', )
    search_fields = ('flow', 'user', 'flow__client')

    def flow__client(self, obj):
        return obj.flow.client
        
admin.site.register(Event, EventAdmin)
admin.site.register(EventActivity, EventActivityAdmin)
admin.site.register(Flow, FlowAdmin)
admin.site.register(FlowActivity, FlowActivityAdmin)
