from django.db import models
from core.models import Client

from chatbot.manager.manager import Manager
from chatbot.manager.constants import CREATION, CHANGE

class Event(models.Model):
    name = models.CharField(max_length=255)
    signal = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name}'

class EventActivity(models.Model):
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    flow_activity = models.ForeignKey('FlowActivity', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.event.name}'

    def save(self, *args, **kwargs):
        if not self.id:
            Manager.signal(self, CREATION)
        else:
            Manager.signal(self, CHANGE)
        super(EventActivity, self).save(*args, **kwargs)

class Flow(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    parent = models.ForeignKey('Flow', on_delete=models.CASCADE, blank=True, null=True)
    description = models.CharField(max_length=32, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    trigger = models.ManyToManyField(Event, blank=True, null=True)

    def __str__(self):
        return f'{self.client}, {self.description}'

class FlowActivity(models.Model):
    flow = models.ForeignKey(Flow, on_delete=models.CASCADE)
    user = models.CharField(max_length=32)
    date = models.DateTimeField(auto_now_add=True)
    finished = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super(FlowActivity, self).save(*args, **kwargs)
        if self.flow.trigger:
            for event in self.flow.trigger.all():
                obj, _ = EventActivity.objects.get_or_create(event=event, flow_activity=self)
                obj.save()

    def __str__(self):
        return f'{self.flow}, {self.user}'

