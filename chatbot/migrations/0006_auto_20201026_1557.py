# Generated by Django 3.0.10 on 2020-10-26 15:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chatbot', '0005_auto_20201026_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventactivity',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chatbot.Event'),
        ),
    ]
