from django.shortcuts import render
from django.utils.timezone import now

from core.models import Client, Access
from chatbot.models import Flow, FlowActivity

from bauth.utils import login_required

from datetime import timedelta

@login_required
def flows_view(request):
    flows = Flow.objects.filter(parent=None)
    for flow in flows:
        flow.chosen = FlowActivity.objects.filter(flow=flow).count()
        flow.children = Flow.objects.filter(parent=flow).count()

    data = {
        'view': 'flows',
        'flows': flows,
    }
    return render(request, 'flows.html', data)

@login_required
def flow_view(request, id):
    parent = Flow.objects.get(id=id)
    flows = Flow.objects.filter(parent=parent)
    for flow in flows:
        flow.chosen = FlowActivity.objects.filter(flow=flow).count()
        flow.children = Flow.objects.filter(parent=flow).count()

    data = {
        'view': 'flow',
        'parent': parent,
        'flows': flows,
    }
    return render(request, 'flow.html', data)

def _get_client_messages(client, **kwargs):
    if 'dt' not in kwargs:
        return FlowActivity.objects.filter(flow__client=client, **kwargs).count()
    return FlowActivity.objects.filter(flow__client=client, date__gte=now()-kwargs['dt']).count()

def _get_client_sales_count(client, **kwargs):
    return 0

def _get_client_sales_value(client, **kwargs):
    return 0

def _get_client_appointments(client, **kwargs):
    return 0

def get_client_analytic_value(client, analytic, **kwargs):
    return {
        'MESSAGES': _get_client_messages(client, **kwargs),
        'SALES_COUNT': _get_client_sales_count(client, **kwargs),
        'SALES_VALUE': _get_client_sales_value(client, **kwargs),
        'APPOINTMENTS': _get_client_appointments(client, **kwargs),
    }.get(analytic, 0)

def get_client_analytic_description(client, analytic, **kwargs):
    return {
        'MESSAGES': _get_client_messages(client, **kwargs),
        'SALES_COUNT': _get_client_sales_count(client, **kwargs),
        'SALES_VALUE': _get_client_sales_value(client, **kwargs),
        'APPOINTMENTS': _get_client_appointments(client, **kwargs),
    }.get(analytic, 0)

@login_required
def analytics_view(request, timeframe='week'):
    dt = timedelta(days=0)
    if timeframe == 'week':
        dt = timedelta(days=8)
    elif timeframe == 'all':
        dt = timedelta(days=9999)
    elif timeframe == 'month':
        dt = timedelta(days=31)
    elif timeframe == 'semester':
        dt = timedelta(days=180)
    elif timeframe == 'year':
        dt = timedelta(days=365)

    access = Access.objects.filter(user=request.user).first()
    analytics = []
    if access:
        analytics = access.client.analytics.all()
        for analytic in analytics:
            analytic.value = get_client_analytic_value(access.client, analytic, dt=dt)

    data = {
        'view': 'analytics',
        'timeframe': timeframe,
        'analytics': analytics,
    }
    return render(request, 'analytics.html', data)

@login_required
def add_flow_view(request):
    data = {
        'view': 'add',
        'id': request.GET.get('v', 0),
    }
    return render(request, 'add.html', data)

@login_required
def edit_flow_view(request):
    data = {
        'view': 'edit',
        'id': request.GET.get('v', 0),
        'flow': Flow.objects.get(id=request.GET.get('v', 0)),
    }
    return render(request, 'edit.html', data)