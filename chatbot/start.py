from handlers import SeleniumClientHandler
import time

import json
import requests
import os

clients = [ SeleniumClientHandler(client_id) for client_id in json.loads(requests.get(f'http://{os.environ["server_address"]}/api/chatbot/get/selenium_clients/').content) ]

while True:
    for client in clients:
        client.init()
    time.sleep(5)