from chatbot.models import Flow, FlowActivity
from core.models import Client, Access
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect

@csrf_exempt
def get_flow_activities_endpoint(request):
    flow, user = request.POST['flow'], request.POST['user']
    return JsonResponse([{
            'id': flow_a.id,
            'flow': flow_a.flow.id,
            'user': flow_a.user,
            'date': flow_a.date,
        } for flow_a in FlowActivity.objects.filter(flow=flow, user=user)], safe=True)

@csrf_exempt
def get_flows_endpoint(request):
    client, parent = request.POST['client'], request.POST['parent']
    return JsonResponse([{
            'id': flow.id,
            'client': flow.client.id,
            'description': flow.description,
            'message': flow.message,
            'trigger': [event.id for event in flow.trigger.all()],
        } for flow in Flow.objects.filter(client=client, parent=parent)], safe=True)

@csrf_exempt
def handle_endpoint(request):
    print(request.POST)
    client, author, msg = request.POST['client'], request.POST['author'], request.POST['msg']
    activity = FlowActivity.objects.filter(user=author, finished=False).first()
    if activity:
        choices = Flow.objects.filter(client=client, parent=activity.flow)

        for index, choice in enumerate(choices):
            if msg == str(index + 1):
                # finish old activity
                activity.finished = True
                activity.save()

                # create new activity
                activity = FlowActivity(flow=choice, user=author)
                activity.save()
                break
    else:
        flow = Flow.objects.filter(client=client, parent=None).first()
        activity = FlowActivity(flow=flow, user=author)
        activity.save()

    response = activity.flow.message + '\n'
    choices = Flow.objects.filter(client=client, parent=activity.flow)
    if not choices:
        activity.finished = True
        activity.save()
    for index, choice in enumerate(choices):
        response += f'{index + 1}. {choice.description}\n'

    return JsonResponse({'response': response}, safe=True)

@csrf_exempt
def selenium_clients_endpoint(request):
    return JsonResponse([client.id for client in Client.objects.filter(active=True)], safe=False)

@csrf_exempt     
def add_flow_endpoint(request):
    id = int(request.POST.get('flow', 0))
    print('request.POST', request.POST)
    client = Access.objects.filter(user=request.user).first().client
    parent = None
    if id:
        parent = Flow.objects.get(id=id)
    flow = Flow(client=client, parent=parent, description=request.POST.get('name', ''), message=request.POST.get('description', ''))
    flow.save()
    if not id:
        return redirect('/flows/')
    return redirect(f'/flows/{id}')

@csrf_exempt     
def edit_flow_endpoint(request):
    id = int(request.POST.get('flow', 0))
    flow = Flow.objects.filter(id=id).first()
    flow.description = request.POST.get('name', '')
    flow.message = request.POST.get('description', '')
    flow.save()
    if not id:
        return redirect('/flows/')
    return redirect(f'/flows/{id}')