from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time
from threading import _start_new_thread
from .client_handler import ClientHandler
def _find_element(f, val, by=By.CSS_SELECTOR, timeout=10):
    out = None
    start = time.time()
    while time.time() - start < timeout and not out:
        try:
            out = f(by, val)
            break
        except:
            pass
        time.sleep(0.1)
    return out

class SeleniumClientHandler(ClientHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.driver = None
        self.first_read_complete = False 
        self.messages = {}

    def start_driver(self):
        options = webdriver.ChromeOptions()
        options.add_argument(r"user-data-dir=driver/data/{}/".format(self.client_id))
        options.binary_location = r"C:\Program Files\Google\Chrome\Application\chrome.exe"
        self.driver = webdriver.Chrome(executable_path=r'driver/chromedriver.exe', options=options)

    def find_element(self, *args, **kwargs):
        return _find_element(self.driver.find_element, *args, **kwargs)

    def find_elements(self, *args, **kwargs):
        return _find_element(self.driver.find_elements, *args, **kwargs)

    def update_author_last_message(self, author, msg):
        print('@update_author_last_message', author, msg)
        msg = msg.replace('\r', '').replace('\n', '').replace(' ', '')
        self.messages[author] = msg

    def reload_messages(self):
        print('@reload_messages')
        chats = self.find_elements("_1MZWu", By.CLASS_NAME)
        for chat in chats:
            content = chat.text.split('\n')
            author = content[0]
            msg = '\n'.join(content[2:])
            if ':' in msg:
                msg = msg.split(':')[1].strip()
                paragraphs = msg.split('\n')
                msg = '\n'.join(paragraphs[:-1])
                if not paragraphs[-1].isdigit():
                    msg += f'\n{paragraphs[-1]}'
            if "digitando" in msg or 'áudio' in msg:
                continue

            msg = msg.replace('\r', '').replace('\n', '').replace(' ', '')
            if author in self.messages:
                if self.messages[author] != msg:
                    self.update_author_last_message(author, msg)
                    self.handle(author, msg)
            else:
                if self.first_read_complete:
                    self.update_author_last_message(author, msg)
                    self.handle(author, msg)
                else:
                    self.update_author_last_message(author, msg)

        if chats:
            self.first_read_complete = True

    def send_message(self, recipient, msg):
        for element in self.find_elements("_1MZWu", By.CLASS_NAME):
            if element.text.split('\n')[0] == recipient:
                element.click()
                break
        
        msg_box = self.find_element('#main > footer > div._3SvgF._1mHgA.copyable-area > div.DuUXI > div > div._1awRl.copyable-text.selectable-text')
        msg_box.click()
        lines = msg.replace('\r', '').split('\n')
        for line in lines:
            if not line: # its \n
                ActionChains(self.driver).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(Keys.ENTER).perform()

            while line not in msg_box.text:
                msg_box.send_keys(line)
                ActionChains(self.driver).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(Keys.ENTER).perform()
        msg_box.send_keys([Keys.ENTER])
        
    def init(self):
        if not self.driver:
            self.start_driver()
            self.driver.get('https://web.whatsapp.com')
            _start_new_thread(self.loop, tuple())

    def loop(self):
        # try:
        while True:
            self.reload_messages()
            time.sleep(0.1)
        # except Exception as e:
        #     print('@loop::exception', repr(e))
            # self.loop()