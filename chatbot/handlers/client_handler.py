import json
import requests
import os

class ClientHandler():
    def __init__(self, client_id):
        self.client_id = client_id

    def handle(self, author, msg):
        print(f'handle@ClientHandler:: ("Client #{self.client_id}", "{repr(msg)}" from {author})')

        if author == 'CHATBOT_DEBUG':
            response = json.loads(requests.post(f'http://{os.environ["server_address"]}/api/chatbot/get/handle/', data={
                "author": author,
                "msg": msg,
                "client": self.client_id,
            }).content)['response']
                                    
            self.update_author_last_message(author, response)
            self.send_message(author, response)
            
    def update_author_last_message(self, author, msg):
        pass

    def reload_messages(self):
        pass

    def send_message(self, recipient, msg):
        pass
        
    def init(self):
        pass

    def loop(self):
        pass


