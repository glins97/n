from django.contrib.auth import authenticate
from django.contrib.auth import login as login_
from django.contrib.auth import logout as logout_
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from bauth.utils import login_required

@csrf_exempt
def login_endpoint(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login_(request, user)
        return redirect(f'/')
    return redirect(f'/?authed=False')

@login_required
def logout_endpoint(request):
    logout_(request)
    return redirect('/')