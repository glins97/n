from django.shortcuts import redirect
from bauth.views import login_view
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def login_required(f, *args, **kwargs):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            print('ACCESS_GRANTED', request.user)
            return f(request, *args, **kwargs)
        else:
            print('LOGIN_REQUIRED', request.user)
            return login_view(request)
    return wrapper
